import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { environment } from './../../environments/environment'
import { from } from 'rxjs';

// models
import { Course } from '../model/course';
import { COURSES } from '../model/courses';
@Injectable({
  providedIn: 'root'
})

export class ApiService {

  baseUri: string = 'http://localhost:4000/v1';
  // headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient, private router: Router) { }


  createEmployee(data): Observable<any> {
    const url = `${this.baseUri}/create`;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }).set('Content-Type', 'application/json')
    };
    let body = JSON.stringify(data);

    return this.http.post(url, data, httpOptions)
      .pipe(
        catchError(this.errorMgmt)
      )
  }


  // Create

  // Get all employees
  getEmployees() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }).set('Content-Type', 'application/json')
    };
    return this.http.get(`${this.baseUri}`, httpOptions);
  }

  // Get employee
  getEmployee(id): Observable<any> {
    const url = `${this.baseUri}/read/${id}`;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }).set('Content-Type', 'application/json')
    };
    return this.http.get(url, httpOptions).pipe(
      map((res: Response) => {
        return res || {}
      }),
      catchError(this.errorMgmt)
    )
  }

  // Update employee
  updateEmployee(id, data): Observable<any> {
    const url = `${this.baseUri}/update/${id}`;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }).set('Content-Type', 'application/json')
    };
    let body = JSON.stringify(data);
    return this.http.put(url, body, httpOptions).pipe(
      catchError(this.errorMgmt)
    )
  }

  // Delete employee
  deleteEmployee(id): Observable<any> {
    let url = `${this.baseUri}/delete/${id}`;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }).set('Content-Type', 'application/json')
    };
    return this.http.delete(url, httpOptions).pipe(
      catchError(this.errorMgmt)
    )
  }


  getCourses(): Course[] {
    return COURSES;
  }

  postCourseToCart(course): Observable<any> {
   
    console.log(`${this.baseUri}/cart/create`, JSON.stringify(course));
    const url = `${this.baseUri}/cart/create`;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }).set('Content-Type', 'application/json')
    };
    let body = JSON.stringify(course);
    return this.http.post(url, body, httpOptions);
  }

  getCartCourses() {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }).set('Content-Type', 'application/json')
    };
    return this.http.get(`${this.baseUri}/cart`);
  }

  // Error handling 
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
