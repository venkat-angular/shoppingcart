import { NgModule } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';

import {Routes , RouterModule} from '@angular/router';
import { WishlistComponent } from './components/wishlist/wishlist.component';
import { CartComponent } from './components/cart/cart.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'wish-list' },
  { path: 'cart', component: CartComponent },
  { path: 'wish-list', component: WishlistComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }